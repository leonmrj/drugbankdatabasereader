package com.medresearchjournal.drugbank;

import com.medresearchjournal.drugbank.generated.DrugType;
import com.medresearchjournal.drugbank.generated.DrugbankType;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.util.Iterator;

/**
 * Created by leonmcdowell on 11/11/15.
 */
public class main {
    public static void main(String[] args) {
        File file = new File(args[0]);

        try {
            JAXBContext context = JAXBContext.newInstance(DrugType.class);
            Unmarshaller unmarshaller = context.createUnmarshaller();
            //DrugbankType drugType = (DrugbankType) unmarshaller.unmarshal(file);
            DrugbankType drugbankType = (DrugbankType) ((javax.xml.bind.JAXBElement) unmarshaller.unmarshal(file)).getValue();
            Iterator itr = drugbankType.getDrug().iterator();
            while (itr.hasNext()) {
                DrugType drug = (DrugType) itr.next();
                System.out.println("Name = " + drug.getName().toString());
                Thread.sleep(200);
            }

        } catch (JAXBException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


    }
}
